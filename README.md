[[_TOC_]]

# SKAO Pipeline Machinery Tutorial

SKAO requires the Pipeline Machinery to track the development to delivery and retain all the released software forever. For achieving this, Gitlab, Nexus and various related automation infrastructure is in place. The SKAO Pipeline Machinery helps the automated processing of self-describing elements at all phases of the SDLC.

# Agenda

- Targeted Outcomes
- Reading Pre-requisites
- Tooling Pre-requisites
- Why Poetry ?
- Managing Dependencies with Poetry
- Need for Standardising the Project Directory Structure
- Makefiles and Makefile targets
- Submodule for Makefiles
- SKAO Gitlab templates
- SKAO Pipeline Machinery Workshop Project

# Targeted Outcomes

Below are the motivations behind having the Pipeline Machinery:

- Standards and Documentation : Ensuring the development guidelines and standards described by SKAO are followed with their documentation in place.
- Shift-left consistency (part of DevSecOps): This requires the operational aspects of software support to move earlier in the Supply Chain using collaborative methods.
- Reference implementations: Ensuring the things that are committed are not broken so that we have bug-free and stable releases.
- Quality checks: Ensuring the software artefacts are complying to quality standards described by SKAO.
- Automated processing of self-describing elements at all phases of the Software Development Life Cycle: Ensuring that each self-describing element at any stage of SDLC can be automated without difficulties.

# Reading Pre-requisites

To get started with this tutorial, below are some references that can help refresh the knowledge of working with Git.

- [Working with Git](https://developer.skatelescope.org/en/latest/tools/git.html?highlight=working%20with%20git)
- [CI/CD](https://developer.skatelescope.org/en/latest/tools/ci-cd.html)
- [CI Process](https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html)
- [Gitlab Global Variables](https://developer.skatelescope.org/en/latest/tools/ci-cd/gitlab-variables.html)
- [Best Practices, Tips and Tricks](https://developer.skatelescope.org/en/latest/tools/ci-cd/best-practices-tips-and-tricks.html)

# Tooling Pre-requisites

To get started with this tutorial, below are some tools that will be required to work with the Pipeline Machinery:

- Python 3.8 or later versions: Install page URL: <https://www.python.org/downloads/>
- Poetry 1.1 or later versions: Install page URL: <https://python-poetry.org/docs/#installation>
- GNU make 4.2 or later versions: Install page URL: <https://www.gnu.org/software/make/>
- Docker 20.10 or later versions: Install page URL: <https://docs.docker.com/engine/install/>

# Why Poetry ?

- Poetry is a great, fresh tool for dependency management and packaging in Python. GitLab pipelines are perfect for continuous integration and deployment. Bringing those two together would help managing Python applications life cycle effectively.
- The traditional approach to dependency management and packaging in Python used setup.py (legacy system) and requirements.txt file which is an outdated approach where the developer needed to specify the dependencies before-hand. However, pyproject.toml is the specified file format of PEP 518 which contains the build system requirements of Python projects. This solves the build-tool dependency chicken and egg problem, i.e. pip can read pyproject.toml and what version of setuptools or wheel one may need. Modern Python packages can contain a pyproject.toml file, first introduced in PEP 518 and later expanded in PEP 517, PEP 621 and PEP 660. This file contains build system requirements and information, which are used by pip to build the package.

E.g.: We have a Python application that we want to lint the code, build the code, test the code, package the code and deploy the code. Also we want to automate this process.
For this the Python application is super simple. It takes data from some structured CSV file, calculates correlation and prints it to the console. This may introduce a dependency say "pandas".
The code also needs the test (a new dependency is pytest).
As we have 5 stages in the worflow (lint, build, test, package and deploy), for each stage we need to install all the required dependencies of our application — pandas and pytest. To manage these dependencies we will use Poetry.

# Managing Dependencies with Poetry

First we need to install Poetry: `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -`

The get-poetry.py script described here will be replaced in Poetry 1.2 by install-poetry.py. From Poetry 1.1.7 onwards, you can already use this script as described as below:

`curl -sSL https://install.python-poetry.org | python3 -`

To manage application dependencies Poetry supports pyproject.toml config file.
This .toml file have three sections:

- [tool.poetry] — fields that describe our application, some of them are required,
- [tool.poetry.dependencies] —a list of all the required packages with version numbers,
- [tool.poetry.dev-dependencies] — a list of the required packages for development purposes: pytest for running unit tests, black for code linting and mypy for static type check.
To install all those dependencies simply run: poetry install
The dependencies will be installed to the virtual environment created and managed by Poetry by creating poetry.lock file which will resolve and install all the dependencies that are listed in pyproject.toml file.
In this way, Poetry handles both the dependencies of our application in one go.

# Need for Standardising the Project Directory Structure

Consistent, simple and clean project structures make the layout predictable for humans and automation a like.  Through consistency, we will be able to implement greater levels of automation within the CI/CD pipelines.  We will be able to assist developers (and DevSecOps) by providing templates and tools that encapsulate our processes, and help apply our best practices and standards with as little Developer overhead as we can muster.
Please refer to the [Standardising Project Structure and Content](https://confluence.skatelescope.org/display/SE/Standardising+Project+Structure+and+Content) for further reading. Here we are only concerned with python related layout for this tutorial. The directories and files that Python project will add apart from the generic layout are : build, dist, poetry.lock and pyproject.toml

# Makefiles and Makefile Targets

- Make is Unix utility that is designed to start execution of a makefile. A makefile is a special file, containing shell commands, that you create and name Makefile. While in the directory containing this makefile, you will type make and the commands in the makefile will be executed. If you create more than one makefile, be certain you are in the correct directory before typing make.
- The Makefiles are designed to be used both standalone - within an SKA project - and in conjunction with the standard GitLab CI pipeline templates, which can be found in ska-telescope/templates-repository
- The Makefiles contain a set of common tagets that standardise the processing and handling of:
  - linting
  - building
  - testing
  - packaging
  - releasing
  - publishing
- Artefacts for Python, OCI Images, Helm Charts, Raw files, Sphinx docs, Conan and Ansible Collections.
- It also contains a simple framework for deploying Helm Charts, and running PyTest suites against Kubernetes both in cluster and on Minikube.
- The general philosophy is to provide the same experience on the desktop for developers, that they will see in the pipelines when it comes to basic actions like lint, build, test etc.
- Without a makefile, this is an extremely time-consuming task.
- As a makefile is a list of shell commands, it must be written for the shell which will process the makefile. A makefile that works well in one shell may not execute properly in another shell. We are overcoming this issue by defining SHELL as bash in Makefile.
- Makefile targets are files that will perform a certain operation specified in it in the same way making the execution platform independent.

# Submodule for Makefiles

This tutorial uses git submodule for makefile which enables the repository to have a centralized collection of the different makefiles containing various makefile targets in one place. This allows you to keep a Git repository as a subdirectory of another Git repository. This lets you clone another repository into your project and keep your commits separate.

To add an example submodule to your repository, the following command should be used:

  ```bash
  git submodule add <URL_of_the_project_to_be_added_as_submodule>
  ```

# SKAO Gitlab Templates

SKAO provides default templates to be used in pipelines for ensuring the smooth execution of operations like linting, building, publishing and scanning which can be accessed [here](https://gitlab.com/ska-telescope/templates-repository/-/tree/master/gitlab-ci/includes)
The needed template can be included in .gitlab-ci.yml file and you are ready to go.

  **Note : The .gitlab-ci.yml file can also be customized as per the requirements. However, we do not recommend it and instead encourage to use makefile to make the customization as it runs the same way in pipeline as well as the developer's local environment.**

# SKAO Pipeline Machinery Workshop Project

This project contains examples and guides for the SKAO Pipeline Machinery Workshop.

This repository contains examples and practices to work with the SKAO Pipeline Machinery including:

- [Pipeline Templates](https://gitlab.com/ska-telescope/templates-repository)
- [Makefile targets](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile)

## Install requirements

Install basic requirements if not present in the machine (please use `sudo` where needed):

```bash
# install common tools like vim awk curl
apt update && apt install -y apt-transport-https curl ca-certificates software-properties-common build-essential gawk vim
# install python and poetry
add-apt-repository ppa:deadsnakes/ppa 
apt update && apt install -y python3.8 git python3.8-venv
curl -sSL https://install.python-poetry.org | python3 -
export PATH="$HOME/.local/bin:$PATH"
# install docker if needed
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce # check that candidate for installation is from the Docker repository
apt install -y docker-ce
```

In case of any problem in the above procedure for the installation of the requirements, it is possible to use a container for doing the exercises with the following commands:

```bash
docker run -v /var/run/docker.sock:/var/run/docker.sock -p 8000:8000 -ti artefact.skao.int/ska-cicd-training-pipeline-machinery:0.2.4 bash
```

## Getting started

First, clone this repository with:

```bash
git clone https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery.git
```

Checkout the `step0` tag and create a branch with your gitlab handler to start the workshop. _(Note: Open the README file on the browser or a separate window from the main branch to follow along)_

```bash
cd ska-cicd-training-pipeline-machinery
git checkout step0
git checkout -b '<workshop-ticket-id>-<gitlab-handle>'
git submodule add https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile.git .make
```

All the exercises are tagged with `Tutorial-Exercise-N-Solution` where `N` is the exercise number to see how the repository should look like. Please feel free to checkout, copy-paste, cherry-pick when you need it.

### Exercise 1: Creating a repository

Let's start with creating a new repository for a python project following SKAO guidelines.

- Navigate to the [Project Structure and Content](https://confluence.skatelescope.org/display/SE/Standardising+Project+Structure+and+Content) page to see the layout.

  ![Project Layout](resources/repo-structure.png)

- Create a `src` folder
- Initialise the python project with poetry

- Read more about the init options and `pyproject.toml` file [here](https://www.python.org/dev/peps/pep-0518/) and [here](https://python-poetry.org/docs/pyproject)

    ```bash
    $ mkdir src
    $ poetry init

    This command will guide you through creating your pyproject.toml config.

    Package name [skao-pipeline-machinery-workshop]:
    Version [0.1.0]:
    Description []:  SKAO Pipeline Machinery Workshop
    Author [Ugur Yilmaz <ugur.yilmaz@skao.int>, n to skip]:
    License []:  BSD-3-Clause
    Compatible Python versions [^3.8]:

    Would you like to define your main dependencies interactively? (yes/no) [yes] no
    Would you like to define your development dependencies interactively? (yes/no) [yes] no
    Generated file

    [tool.poetry]
    name = "skao-pipeline-machinery-workshop"
    version = "0.1.0"
    description = "SKAO Pipeline Machinery Workshop"
    authors = ["Ugur Yilmaz <ugur.yilmaz@skao.int>"]
    license = "BSD-3-Clause"

    [tool.poetry.dependencies]
    python = "^3.10"

    [tool.poetry.dev-dependencies]

    [build-system]
    requires = ["poetry-core"]
    build-backend = "poetry.core.masonry.api"


    Do you confirm generation? (yes/no) [yes]
    ```

- Optional: Set up poetry to create virtual environments in project

  ```bash
  poetry config virtualenvs.in-project true
  ```

- Add a `.gitignore` file (An example for python could be used from [here](https://github.com/github/gitignore/blob/master/Python.gitignore))
- Open the Command Pallette on VS Code (Ctrl+Shift+P) and type ">gitignore" and select the option that appears i.e. Git:Add gitignore
- It will ask for another input which is the type of template in which select Python.

- Make sure that you add and commit the .gitmodules file and .make directory

  ```bash
  git add .gitmodules .make
  git commit -s # write a commit message then press ':wq!
  ```

- Create a `Makefile` and include the \*.mk files in your Makefile

  ```bash
  # include OCI Images support
  include .make/oci.mk
  
  # include k8s support
  include .make/k8s.mk
  
  # include Helm Chart support
  include .make/helm.mk
  
  # Include Python support
  include .make/python.mk
  
  # include raw support
  include .make/raw.mk

  # include core make support
  include .make/base.mk

  # include your own private variables for custom deployment configuration
  -include PrivateRules.mak

  ```

- Create a folder under `src/` with the same name as repository name `src/ska_cicd_training_pipeline_machinery`
- Create a `main.py` file under `src/ska_cicd_training_pipeline_machinery` for a simple [FastAPI](https://fastapi.tiangolo.com/) app
- Install the fastapi dependency with "all" as parameter to tell the installation to install all the dependencies of the fastapi package as it itself defines its own dependencies.

  ```bash
  poetry add fastapi[all]
  ```

- Please make sure `fastapi` is added to the [tool.poetry.dependencies] section in pyproject.toml file

  ```bash
  fastapi = {extras = ["all"], version = "^0.110.1"}
  ```

- Add a root page (below code) into `main.py` file

  ```python
  from fastapi import FastAPI

  app = FastAPI()


  @app.get("/")
  async def root():
      return {"message": "Hello World"}

  ```

- Add a makefile target for testing the app

  ```bash
  example-start-server:
    uvicorn src.ska_cicd_training_pipeline_machinery.main:app --reload
  ```

  We namespace any make targets we introduce so that it's easy to see where they are coming from and won't be conflicted with any included make targets.

- Open a terminal window and go into the virtual environment by typing `poetry shell`
- Test it by running `make example-start-server` and navigating to <http://127.0.0.1:8000>
You should be able to see the following output at the URL opened in the browser:
  
  ```bash
  {"message":"Hello World"}
  ```

- Exit by typing `Ctrl+C` into the terminal

The file structure of the repository after completing the Solution for Exercise 1 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-1-Solution)

### Exercise 2: Working with Makefile submodule

- Test the linting using the make targets by calling `make python-lint`. We should see some errors about missing packages:

  ```bash
  $ make python-lint
    isort --check-only --profile black -w 79  src/ tests/
    bash: isort: command not found
    make: *** [.make/python.mk:88: python-do-lint] Error 127
  ```

- Add missing packages

  ```bash
  poetry add --dev isort black flake8 pylint pylint-junit
  ```

- Run linting again

  ```bash
  $ make python-lint
    isort --check-only --profile black -w 79  src/ tests/
    Broken 1 paths
    black --check --line-length 79  src/ tests/
    Usage: black [OPTIONS] SRC ...
    Try 'black -h' for help.

    Error: Invalid value for 'SRC ...': Path 'tests/' does not exist.
    make: *** [.make/python.mk:89: python-do-lint] Error 2
  ```

- Now we are missing tests! Let's add a simple test by creating a `tests/` folder. Note just adding `tests/` folder should make the linting run but let's continue with adding a simple test by creating a new file `test_main.py` file in `tests/` folder with following contents:

  ```python
  """Simple FASTAPI App for workshop purposes

  Returns:
    None
  """

  from fastapi.testclient import TestClient

  from ska_cicd_training_pipeline_machinery.main import app

  client = TestClient(app)

  def test_read_main():
    """Unit test for the root path "/" """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}

  ```

- Add the testing dependencies

  ```bash
  poetry add --dev pytest pytest-cov
  ```

- Get back to linting:

  ```bash
  $ make python-lint
  isort --check-only --profile black -w 79  src/ tests/
  black --check --line-length 79  src/ tests/
  would reformat src/ska_cicd_training_pipeline_machinery/main.py
  would reformat tests/test_main.py
  Oh no! 💥 💔 💥
  2 files would be reformatted.
  make: *** [.make/python.mk:89: python-do-lint] Error 1
  ```

- Run `make python-format` to ensure that formatting is okay

  ```bash
  make python-format
  isort --profile black -w 79  src/ tests/
  black --line-length 79  src/ tests/
  reformatted src/ska_cicd_training_pipeline_machinery/main.py
  reformatted tests/test_main.py
  All done! ✨ 🍰 ✨
  2 files reformatted.
  ```

- Get back to linting:

  ```bash
  $ make python-lint
  ...
  isort --check-only --profile black -w 79  src/ tests/
  black --check --line-length 79  src/ tests/
  All done! ✨ 🍰 ✨
  2 files would be left unchanged.
  flake8 --show-source --statistics  src/ tests/
  pylint --output-format=parseable  src/ tests/   | tee build/code_analysis.stdout
  ************* Module src.main
  src/main.py:1: [C0114(missing-module-docstring), ] Missing module docstring
  src/main.py:7: [C0116(missing-function-docstring), root] Missing function or method docstring
  ************* Module tests.test_main
  tests/test_main.py:1: [C0114(missing-module-docstring), ] Missing module docstring
  tests/test_main.py:8: [C0116(missing-function-docstring), test_read_main] Missing function or method docstring

  -----------------------------------
  Your code has been rated at 6.36/10

  pylint --output-format=pylint_junit.JUnitReporter  src/ tests/   > build/reports/linting-python.xml
  make: *** [.make/python.mk:92: python-do-lint] Error 16
  ```

- Fix the issues you see with the above command. The target rate should be 10.0/10
- Run the tests with `make python-test`.

  ```bash
  $ make python-test
    pytest 6.2.5
    PYTHONPATH=./src:/app/src  pytest  \
    --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml tests/
    ================================================================== test session starts ===================================================================
    platform linux -- Python 3.8.10, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
    rootdir: /root/ska-cicd-training-pipeline-machinery
    plugins: cov-3.0.0, anyio-3.5.0
    collected 1 item

    tests/test_main.py .                                                                                                                               [100%]

    ==================================================================== warnings summary ====================================================================
    tests/test_main.py::test_read_main
      /root/ska-cicd-training-pipeline-machinery/tests/test_main.py:17: UserWarning: no api version is specified!
        warnings.warn(UserWarning("no api version is specified!"))

    -- Docs: https://docs.pytest.org/en/stable/warnings.html
    ------------------------------ generated xml file: /root/ska-cicd-training-pipeline-machinery/build/reports/unit-tests.xml -------------------------------

    ---------- coverage: platform linux, python 3.8.10-final-0 -----------
    Name                                               Stmts   Miss  Cover   Missing
    --------------------------------------------------------------------------------
    src/ska_cicd_training_pipeline_machinery/main.py       5      0   100%
    --------------------------------------------------------------------------------
    TOTAL                                                  5      0   100%
    Coverage XML written to file build/reports/code-coverage.xml

    ============================================================== 1 passed, 1 warning in 0.40s ==============================================================
  ```

- Let's build our python package. First, install building dependencies

  ```bash
  poetry add --dev build
  ```

  Note that, if you are using `poetry` like we do here you could also run `poetry build` to use poetry CLIs build functionality. [build](https://github.com/pypa/build) package we installed is a PEP-517 compliant simple build frontend which is a dependency from the makefile submodules that still calls poetry-core on the backend as defined by `pyproject.toml` file.

- Build our python package

  ```bash
  make python-build
  ```

The file structure of the repository after completing the Solution for Exercise 2 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-2-Solution)

### Exercise 3: Adding the pipeline

- Create `.gitlab-ci.yml` file
- Define the image and submodule strategy to ensure makefile submodule is also pulled

  ```yaml
  default:
    image: $SKA_K8S_TOOLS_BUILD_DEPLOY

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  ```

- Add the stages

  ```yaml
  stages:
    - lint
    - build
    - test  
    - deploy
    - integration
    - staging
    - publish
    - pages
    - scan
  ```

  The python template job can be found [here](https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/python.gitlab-ci.yml). The CI/CD templates are using the makefile targets that were expected to be present in the repository for different stages and jobs. For example, in order to run linting it runs `make python-lint`.

- Add a `__init__.py` file in the `tests/` folder as the test pipeline expects this file to be present.  This can be blank at this stage.

  ```bash
  touch tests/__init__.py
  ```

- Include the CI templates for python support and finalisers for the badges and workflow rules

  ```yaml
  include:
    # Python
    - project: "ska-telescope/templates-repository"
      file: "gitlab-ci/includes/python.gitlab-ci.yml"
      # .post step finalisers eg: badges
    - project: "ska-telescope/templates-repository"
      file: "gitlab-ci/includes/finaliser.gitlab-ci.yml"
  ```

- Push the code we have so far.

  ```bash
  git add . && git commit && git push -u origin HEAD
  ```

Please note that if an error related to detetion of incompatible Python version is found in the pipeline, please update the version under the [tool.poetry.dependencies] section in pyproject.toml file with the respective version which is found in the pipeline error message.

The file structure of the repository after completing the Solution for Exercise 3 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-3-Solution)

### Exercise 4: Help and Customising Makefile Targets

Until this point we have a basic python app that's been linted, unit-tested, built and published in the pipeline using the makefile targets that are provided.

There is often a need to customise these steps for our use due to team level policies or repository requirements.

In this exercise, we will learn more about how to get help and customise make targets.

- To get brief help on existing make targets run the below command which will output every make target with available variables.

  ```bash
  $ make help

  ------
  SECTION: Makefile
  MAKE TARGETS:

  MAKE VARS (+defaults):

  ------
  SECTION: ansible
  MAKE TARGETS:
  ansible-lint                   lint the Ansible collections
  ansible-publish                publish the ansible collections to the repository

  MAKE VARS (+defaults):

  ------
  SECTION: dev
  MAKE TARGETS:
  dev-git-hooks                  activate git-hooks if available in project
  dev-vscode                     copy in vscode config if available in project

  MAKE VARS (+defaults):

  ------
  SECTION: docs
  MAKE TARGETS:
  docs-build                     Build docs - must pass sub command
  docs-help                      help for docs

  MAKE VARS (+defaults):

  ------
  SECTION: release
  MAKE TARGETS:
  bump-major-release             bump major release
  bump-minor-release             bump minor release
  bump-patch-release             bump patch release
  check-release                  check if there's a tag in Git for the current version
  check-status                   check if there are still outstanding changes
  git-create-tag                 create git tag for current version
  git-delete-tag                 delete git tag for current VERSION
  git-push-tag                   push git and tags
  helm-set-release               set the Helm Chart version and appVersion
  python-set-release             set the Python package version
  set-release                    Set the release from $VERSION in .release
  show-version                   Show current release version

  MAKE VARS (+defaults):
  CHANGELOG_CONFIG               ".chglog/config.yml"
  CHANGELOG_FILE                 CHANGELOG.md
  CHANGELOG_TEMPLATE             ".chglog/CHANGELOG.tpl.md"
  RELEASE_CONTEXT_DIR ?=

  ------
  SECTION: oci
  MAKE TARGETS:
  oci-boot-into-tools            Boot the pytango-builder image with the project directory mounted to /app
  oci-build-all                  build all the OCI_IMAGES image (from /images/*)
  oci-build                      build the OCI_IMAGE image (from /images/<OCI_IMAGE dir>)
  oci-lint                       lint the OCI image
  oci-publish-all                Publish all OCI Images in OCI_IMAGES_TO_PUBLISH
  oci-publish                    publish the OCI_IMAGE to the CAR_OCI_REGISTRY_HOST registry from CI_REGISTRY
  oci-scan-all                   Scan all OCI Images in OCI_IMAGES_TO_PUBLISH (must run inside docker.io/aquasec/trivy:latest)
  oci-scan                       scan the OCI_IMAGE (must run inside docker.io/aquasec/trivy:latest)

  MAKE VARS (+defaults):
  CAR_OCI_REGISTRY_HOST          ## OCI Image Registry
  OCI_BUILDER                    docker ## Image builder eg: docker, or podman
  OCI_BUILD_ADDITIONAL_ARGS      ## Additional build argument string
  OCI_IMAGE                      $(PROJECT_NAME) ## Default Image (from /images/<OCI_IMAGE dir>)
  OCI_IMAGES                     $(OCI_IMAGE_DIRS) ## Images to lint and build
  OCI_IMAGES_TO_PUBLISH          $(OCI_IMAGES) ## Images to publish
  OCI_IMAGE_BUILD_CONTEXT        .  ## Image build context directory, relative to /images/<image dir>
  OCI_IMAGE_FILE_PATH            Dockerfile ## Image recipe file
  OCI_LINTER                     registry.gitlab.com/pipeline-components/hadolint:0.16.0
  OCI_SKIP_PUSH                  ## OCI Skip the push
  OCI_TOOLS_IMAGE                artefact.skao.int/ska-tango-images-pytango-builder:9.3.12

  ------
  SECTION: raw
  MAKE TARGETS:
  raw-package-all                package all raw artefacts
  raw-package                    build the raw package
  raw-publish-all                publish all raw artefacts to the repository
  raw-publish                    publish the raw artefact to the repository

  MAKE VARS (+defaults):
  RAW_OUT_DIR                    build/raw## output directory for building Raw packages
  RAW_PKGS                       $(RAW_OUT_PKG_DIRS)
  RAW_PKGS                       $(RAW_PKG_DIRS)

  ------
  SECTION: conan
  MAKE TARGETS:
  conan-package-all              package all conan artefacts
  conan-package                  build the conan package
  conan-publish-all              publish all conan artefacts to the repository
  conan-publish                  publish the conan artefact to the repository

  MAKE VARS (+defaults):
  CONAN_CHANNEL                  stable# channel that will be used to build with conan
  CONAN_OUT_DIR                  $(BASE)/build/## output directory for building conan packages
  CONAN_PKG                      $(PROJECT_NAME)
  CONAN_PKGS                     $(CONAN_PKG_DIRS)
  CONAN_PKGS_TO_PUBLISH          $(CONAN_PKGS)
  CONAN_USER                     marvin# user that will be used to build with conan

  ------
  SECTION: cpp
  MAKE TARGETS:
  cpp-build                      build the C/C++ binary
  cpp-publish                    publish the conan artefact to the repository
  cpp-test                       test the C/C++ binary

  MAKE VARS (+defaults):

  ------
  SECTION: python
  MAKE TARGETS:
  join-lint-reports              Join linting report (chart and python)
  python-build                   build the Python package
  python-format                  format the Python code
  python-lint                    lint the Python code
  python-publish                 publish the Python artefact to the repository
  python-test                    test the Python package

  MAKE VARS (+defaults):
  PYTHON_BUILD_TYPE              non_tag_pyproject ## used to differentiate build types
  PYTHON_LINT_TARGET             src/ tests/  ## Paths containing python to be formatted and linted
  PYTHON_PUBLISH_PASSWORD        ## Password used to publish
  PYTHON_PUBLISH_URL             ## URL to publish
  PYTHON_PUBLISH_USERNAME        ## Username used to publish
  PYTHON_RUNNER                  ## use to specify command runner, e.g. "poetry run" or "python -m"
  PYTHON_SWITCHES_FOR_BLACK      --line-length 79 ## Custom switches added to black
  PYTHON_SWITCHES_FOR_FLAKE8     ## Custom switches added to flake8
  PYTHON_SWITCHES_FOR_ISORT      -w 79 ## Custom switches added to isort
  PYTHON_SWITCHES_FOR_PYLINT     ## Custom switches added to pylint
  PYTHON_TEST_FILE               tests/ ## Option pytest test file
  PYTHON_VARS_AFTER_PYTEST       ## used to include optional pytest flags
  PYTHON_VARS_BEFORE_PYTEST      PYTHONPATH=./src:/app/src## used to include needed argument variables to pass to pytest if necessary

  ------
  SECTION: k8s
  MAKE TARGETS:
  k8s-bounce                     restart all statefulsets by scaling them down and up
  k8s-chart-version              get the latest versin number for helm chart K8S_CHART
  k8s-clean                      clean out temp files
  k8s-delete-namespace           delete the kubernetes namespace
  k8s-dep-update                 update dependencies for every charts in the env var K8S_CHARTS
  k8s-describe                   describe Pods executed from Helm chart
  k8s-get-size-images            get a list of images together with their size (both local and compressed) in the namespace KUBE_NAMESPACE
  k8s-install-chart              install the helm chart with name HELM_RELEASE and path K8S_UMBRELLA_CHART_PATH on the namespace KUBE_NAMESPACE
  k8s-interactive                run the ipython command in the itango console available with the tango-base chart
  k8s-kubeconfig                 export current KUBECONFIG as base64 ready for KUBE_CONFIG_BASE64
  k8s-namespace                  create the kubernetes namespace
  k8s-pod-versions               lists the container images used for particular pods
  k8s-podlogs                    show Helm chart POD logs
  k8s-reinstall-chart            reinstall test-parent helm chart on the namespace ska-tango-examples
  k8s-smoke-test                 wait target
  k8s-template-chart             template the helm chart with name HELM_RELEASE and path K8S_UMBRELLA_CHART_PATH on the namespace KUBE_NAMESPACE
  k8s-test                       run the defined test cycle against Kubernetes
  k8s-uninstall-chart            uninstall the helm chart with name HELM_RELEASE on the namespace KUBE_NAMESPACE
  k8s-upgrade-chart              upgrade the test-parent helm chart on the namespace ska-tango-examples
  k8s-vars                       Which kubernetes are we connected to
  k8s-wait                       wait for Jobs and Pods to be ready in KUBE_NAMESPACE
  k8s-watch                      watch all resources in the KUBE_NAMESPACE

  MAKE VARS (+defaults):
  CAR_OCI_REGISTRY_HOST          artefact.skao.int
  COUNT                          1## amount of repetition for pytest-repeat
  FILE                           ##this variable sets the execution of a single file in the pytest
  HELM_RELEASE                   test## Helm release
  K8S_CHART                      $(NAME)## selected chart
  K8S_CHARTS                     $(K8S_CHART) ## list of charts
  K8S_CHART_PARAMS               ## Additional helm chart parameters
  K8S_HELM_REPOSITORY            https://artefact.skao.int/repository/helm-internal
  K8S_TEST_IMAGE_TO_TEST         $(CAR_OCI_REGISTRY_HOST)/$(NAME):$(VERSION)## docker image that will be run for testing purpose
  K8S_TEST_RUNNER_ADD_ARGS       --limits='cpu=1000m,memory=500Mi' --requests='cpu=900m,memory=400Mi' ## Additional arguments passed to the K8S test runner
  K8S_TEST_TEST_COMMAND          cd .. && $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) \
  K8S_TIMEOUT                    360s ## kubectl wait timeout - 6 minutes
  K8S_UMBRELLA_CHART_PATH        ./charts/$(K8S_CHART)/ ## path to umbrella chart used for testing
  KUBE_APP                       $(NAME)## Kubernetes app label name
  KUBE_NAMESPACE                 $(NAME)## Kubernetes Namespace
  MARK                           all## this variable sets the mark parameter in the pytest

  ------
  SECTION: help
  MAKE TARGETS:
  help                           show this help.
  long-help                      show detailed help.

  MAKE VARS (+defaults):

  ------
  SECTION: make
  MAKE TARGETS:
  make                           Update the .make git submodule
  submodule                      update git submodules

  MAKE VARS (+defaults):

  ------
  SECTION: base
  MAKE TARGETS:

  MAKE VARS (+defaults):

  ------
  SECTION: release
  MAKE TARGETS:
  bump-major-release             bump major release
  bump-minor-release             bump minor release
  bump-patch-release             bump patch release
  check-release                  check if there's a tag in Git for the current version
  check-status                   check if there are still outstanding changes
  git-create-tag                 create git tag for current version
  git-delete-tag                 delete git tag for current VERSION
  git-push-tag                   push git and tags
  helm-set-release               set the Helm Chart version and appVersion
  python-set-release             set the Python package version
  set-release                    Set the release from $VERSION in .release
  show-version                   Show current release version

  MAKE VARS (+defaults):
  CHANGELOG_CONFIG               ".chglog/config.yml"
  CHANGELOG_FILE                 CHANGELOG.md
  CHANGELOG_TEMPLATE             ".chglog/CHANGELOG.tpl.md"
  RELEASE_CONTEXT_DIR ?=

  ------
  SECTION: docs
  MAKE TARGETS:
  docs-build                     Build docs - must pass sub command
  docs-help                      help for docs

  MAKE VARS (+defaults):

  ------
  SECTION: make
  MAKE TARGETS:
  make                           Update the .make git submodule
  submodule                      update git submodules

  MAKE VARS (+defaults):

  ------
  SECTION: dev
  MAKE TARGETS:
  dev-git-hooks                  activate git-hooks if available in project
  dev-vscode                     copy in vscode config if available in project

  MAKE VARS (+defaults):

  ------
  SECTION: help
  MAKE TARGETS:
  help                           show this help.
  long-help                      show detailed help.

  MAKE VARS (+defaults):

  ------
  SECTION: helm
  MAKE TARGETS:
  helm-build                     build the Helm Charts and publish to the GitLab repository
  helm-lint                      lint the Helm Charts
  helm-publish                   publish the Helm Charts to the repository

  MAKE VARS (+defaults):
  HELM_BUILD_PUSH_SKIP ?=
  HELM_CHARTS                    $(HELM_CHART_DIRS)
  HELM_CHARTS_CHANNEL            dev  ## Helm Chart Channel for GitLab publish
  HELM_CHARTS_TO_PUBLISH         $(HELM_CHARTS)
  HELM_YQ_VERSION                4.14.1## yq version to install
  ```

  - To get help on a specific section we can use `make help <section-name>`

  ```bash
  $ make help python

  ------
  SECTION: python
  MAKE TARGETS:
  join-lint-reports              Join linting report (chart and python)
  python-build                   build the Python package
  python-format                  format the Python code
  python-lint                    lint the Python code
  python-publish                 publish the Python artefact to the repository
  python-test                    test the Python package

  MAKE VARS (+defaults):
  PYTHON_BUILD_TYPE              non_tag_pyproject ## used to differentiate build types
  PYTHON_LINT_TARGET             src/ tests/  ## Paths containing python to be formatted and linted
  PYTHON_PUBLISH_PASSWORD        ## Password used to publish
  PYTHON_PUBLISH_URL             ## URL to publish
  PYTHON_PUBLISH_USERNAME        ## Username used to publish
  PYTHON_RUNNER                  ## use to specify command runner, e.g. "poetry run" or "python -m"
  PYTHON_SWITCHES_FOR_BLACK      --line-length 79 ## Custom switches added to black
  PYTHON_SWITCHES_FOR_FLAKE8     ## Custom switches added to flake8
  PYTHON_SWITCHES_FOR_ISORT      -w 79 ## Custom switches added to isort
  PYTHON_SWITCHES_FOR_PYLINT     ## Custom switches added to pylint
  PYTHON_TEST_FILE               tests/ ## Option pytest test file
  PYTHON_VARS_AFTER_PYTEST       ## used to include optional pytest flags
  PYTHON_VARS_BEFORE_PYTEST      PYTHONPATH=./src:/app/src## used to include needed argument variables to pass to pytest if necessary
  ```

- The above command shows that we have a couple of variables we can use to configure out different stages. It also shows that are the default values for those variables.

- For example, we may want to disable pytest warnings for the tests or stop after a number of failures etc. So, let's try it.
- First, let's show a warning in our tests. Change the `tests/test_main.py` to:

  ```python
  """
  Simple FASTAPI App for workshop purposes

  Returns:
    None
  """
  import warnings

  from fastapi.testclient import TestClient

  from ska_cicd_training_pipeline_machinery.main import app

  client = TestClient(app)

  def api():
    """API version warning"""
    warnings.warn(UserWarning("no api version is specified!"))
    return 1


  def test_read_main():
    """Unit test for the root path "/" """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}
    assert api() == 1
  ```

- Don't forget to format and lint: `make python-format && make python-lint`

- Run the tests:

  ```bash
  $ make python-test
    pytest 6.2.5
    PYTHONPATH=./src:/app/src  pytest  \
    --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml tests/
    ================================================================== test session starts ===================================================================
    platform linux -- Python 3.8.10, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
    rootdir: /root/ska-cicd-training-pipeline-machinery
    plugins: cov-3.0.0, anyio-3.5.0
    collected 1 item

    tests/test_main.py .                                                                                                                               [100%]

    ==================================================================== warnings summary ====================================================================
    tests/test_main.py::test_read_main
      /root/ska-cicd-training-pipeline-machinery/tests/test_main.py:17: UserWarning: no api version is specified!
        warnings.warn(UserWarning("no api version is specified!"))

    -- Docs: https://docs.pytest.org/en/stable/warnings.html
    ------------------------------ generated xml file: /root/ska-cicd-training-pipeline-machinery/build/reports/unit-tests.xml -------------------------------

    ---------- coverage: platform linux, python 3.8.10-final-0 -----------
    Name                                               Stmts   Miss  Cover   Missing
    --------------------------------------------------------------------------------
    src/ska_cicd_training_pipeline_machinery/main.py       5      0   100%
    --------------------------------------------------------------------------------
    TOTAL                                                  5      0   100%
    Coverage XML written to file build/reports/code-coverage.xml

    ============================================================== 1 passed, 1 warning in 0.35s ==============================================================
  ```

  We can see that there's a `warnings summary` section which gives details about the warnings.

- Define `PYTHON_VARS_AFTER_PYTEST` variable to ignore pytest warnings in the `Makefile`

  ```bash
  PYTHON_VARS_AFTER_PYTEST=--disable-pytest-warnings
  ```

  _Note: for local experimentation or one-time runs you could define the variables when calling the make targets directly, i.e. `make python-test PYTHON_VARS_AFTER_PYTEST=--disable-pytest-warnings`_

- Run the tests again

  ```bash
  $ make python-test PYTHON_VARS_AFTER_PYTEST=--disable-pytest-warnings
    pytest 6.2.5
    PYTHONPATH=./src:/app/src  pytest --disable-pytest-warnings \
    --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml tests/
    ================================================================== test session starts ===================================================================
    platform linux -- Python 3.8.10, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
    rootdir: /root/ska-cicd-training-pipeline-machinery
    plugins: cov-3.0.0, anyio-3.5.0
    collected 1 item

    tests/test_main.py .                                                                                                                               [100%]

    ------------------------------ generated xml file: /root/ska-cicd-training-pipeline-machinery/build/reports/unit-tests.xml -------------------------------

    ---------- coverage: platform linux, python 3.8.10-final-0 -----------
    Name                                               Stmts   Miss  Cover   Missing
    --------------------------------------------------------------------------------
    src/ska_cicd_training_pipeline_machinery/main.py       5      0   100%
    --------------------------------------------------------------------------------
    TOTAL                                                  5      0   100%
    Coverage XML written to file build/reports/code-coverage.xml

    ============================================================== 1 passed, 1 warning in 0.35s ==============================================================

  ```

  Now, we don't see the summary for warnings.

  Similarly, we may even turn the warning into error or provide any other option to `pytest`(or any of the tooling that are used in makefile targets!)

  Alternatively, we could also configure the tooling used by the makefile targets by introducing configuration options in the `pyproject.toml` file or `pytest.ini` file. **The repository configuration is always takes precendence!**

  ```
  # pyproject.toml (preferred approach when using config files)
  [tool.pytest.ini_options]
  filterwarnings = [
      "error",
      "ignore::UserWarning",
      # note the use of single quote below to denote "raw" strings in TOML
      'ignore:function ham\(\) is deprecated:DeprecationWarning',
  ]
  ```

  or

  ```
  # pytest.ini
  [pytest]
  filterwarnings =
      error
      ignore::UserWarning
      ignore:function ham\(\) is deprecated:DeprecationWarning
  ```

- To see the actual comments that will run when we call a make target, use `--dry` option. This will substitute any variables with their actual values and will only show the final command that will run.

  ```bash
  $ make python-test --dry
  ...
  pytest --version -c /dev/null
  mkdir -p build
  PYTHONPATH=./src:/app/src  pytest --disable-pytest-warnings \
  --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml tests/
  ```

  - To get detailed help, use `make long-help` command

  ```bash
  $ make long-help
  ...
  Enter a Makefile section name or press enter for all  🔎 [ .make/ansible .make/dev .make/docs .make/release .make/oci .make/raw .make/conan .make/cpp .make/python .make/k8s .make/help .make/make .make/ .make/release .make/docs .make/make .make/dev .make/help .make/helm]: python

  -------
  TARGET:   python-format
  SYNOPSIS: make python-format
  HOOKS:    python-pre-format, python-post-format
  VARS:
        PYTHON_RUNNER=<python executor> - defaults to empty, but could pass something like python -m
        PYTHON_LINT_TARGET=<file or directory path to Python code> - default 'src/ tests/'
        PYTHON_SWITCHES_FOR_ISORT=<additional switches to pass to isort>
        PYTHON_SWITCHES_FOR_BLACK=<additional switch to pass to black>

  Reformat project Python code in the given directories/files using black and isort.

  -------
  TARGET:   python-lint
  SYNOPSIS: make python-lint
  HOOKS:    python-pre-lint, python-post-lint
  VARS:
        PYTHON_RUNNER=<python executor> - defaults to empty, but could pass something like python -m
        PYTHON_LINT_TARGET=<file or directory path to Python code> - default 'src/ tests/'
        PYTHON_SWITCHES_FOR_ISORT=<additional switches to pass to isort>
        PYTHON_SWITCHES_FOR_BLACK=<additional switch to pass to black>
        PYTHON_SWITCHES_FOR_FLAKE8=<additional switch to pass to flake8>
        PYTHON_SWITCHES_FOR_PYLINT=<additional switch to pass to pylint>

  Lint check project Python code in the given directories/files using black, isort, flake8 and pylint.

  -------
  TARGET:   python-build
  SYNOPSIS: make python-build
  HOOKS:    python-pre-build, python-post-build
  VARS:
        PYTHON_RUNNER=<python executor> - defaults to empty, but could pass something like python -m
        PYTHON_BUILD_TYPE=[tag_setup|non_tag_setup|non_tag_pyproject|tag_pyproject]

  Build the nominated package type for project Python code, and decorate the package
  with the SKAO metadata required for publishing to the Central Artefact Repository.
  Types:  .
    tag_setup:         python3 setup.py sdist bdist_wheel
    non_tag_setup:     python3 setup.py egg_info -b+dev[.${CI_COMMIT_SHORT_SHA}] sdist bdist_wheel
    non_tag_pyproject: python3 -m build (pyproject.toml package version set to +dev[.${CI_COMMIT_SHORT_SHA}])
    tag_pyproject:     python3 -m build

  -------
  TARGET:   python-test
  SYNOPSIS: make python-test
  HOOKS:    python-pre-test, python-post-test
  VARS:
        PYTHON_RUNNER=<python executor> - defaults to empty, but could pass something like python -m
        PYTHON_TEST_FILE=<paths and/or files for testing> - defaults to tests/unit/
        PYTHON_VARS_BEFORE_PYTEST=<environment variables defined before pytest in run> - default empty
        PYTHON_VARS_AFTER_PYTEST=<additional switches passed to pytest> - default empty

  Run pytest against the tests defined in ./tests.  By default, this will pickup any pytest
  specific configuration set in pytest.ini, setup.cfg etc. located in ./tests

  -------
  TARGET:   python-publish
  SYNOPSIS: make python-publish
  HOOKS:    python-pre-publish, python-post-publish
  VARS:
        PYTHON_RUNNER=<python executor> - defaults to empty, but could pass something like python -m
        PYTHON_PUBLISH_USERNAME=<twine user> - default empty
        PYTHON_PUBLISH_PASSWORD=<twine user password> - default empty
        PYTHON_PUBLISH_URL=<repository URL> - default empty

  Run twine to publish artefacts built in the project dist/ directory.

  -------
  TARGET:   python-exportlock
  SYNOPSIS: make python-exportlock
  HOOKS:    none
  VARS:     none

  Run poetry export to generate requirements.txt and requirements-dev.txt based on pyproject.toml.
  ```

  The long-help command will give detailed description on each target, provide its synopsis, hooks and variables.

  - We have already seen the variable usage above but there's also `pre` and `post` hooks that we can use to further customise and extend our workflow. The `pre` hooks are executed right before the actual make target and the `post` target is executed right afterwards. By default they are both empty.
  - To see a debug information or provide additional logging let's use `python-pre-test` hook.
  - Add following snippet to your `Makefile`

  ```bash
    python-pre-test:
      @echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) with $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST); \
      $(PYTHON_TEST_FILE)";\
      echo "Python Version:";\
      python -V;\
      echo "-----------------------";\
      echo "Environment variables:";\
      printenv;\
      echo "-----------------------"
  ```

- Run `make python-test` again to see the output of the above right before the actual tests are run

  ```bash
  $ make python-test
  ...
  python-pre-test: running with: PYTHONPATH=./src:/app/src with  pytest --disable-pytest-warnings;  tests/
  Python Version:
  Python 3.8.7
  -----------------------
  Environment variables:
  SHELL=/usr/bin/fish
  COLORTERM=truecolor
  PYENV_SHELL=fish
  TERM_PROGRAM_VERSION=1.62.3
  WSL_DISTRO_NAME=Ubuntu
  WT_SESSION=87ad6017-6c70-43a8-a490-14d85f56d1d6
  _tide_right_prompt_display_var=_tide_right_prompt_display_348945
  MAKE_TERMOUT=/dev/pts/35
  SSH_AUTH_SOCK=/tmp/ssh-rHnATmQBlwEF/agent.349068
  POETRY_ACTIVE=1
  ...
  VSCODE_IPC_HOOK_CLI=/tmp/vscode-ipc-f4a476a7-a279-496e-9036-f112be4cb0d7.sock
  _=/usr/bin/printenv
  -----------------------
  pytest 6.2.5
  ```

- **It's important that as the ci/cd templates are just calling the makefile targets that are defined in the repository as you would call in your local development, you don't have to configure anything in the `gitlab-ci.yml` file itself!**
- Explore other variables and hooks and experiment!

The file structure of the repository after completing the Solution for Exercise 4 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-4-Solution)

### Exercise 5: Add OCI Image Support

Let's add container support to ship our software so that it can be run as standalone in k8s environment.

- Add `Dockerfile` to the root folder of the repository.
  
  ```dockerfile
  # Use the official Python 3.10 image as the base image
  FROM python:3.10

  # Set the working directory to /app within the container
  WORKDIR /app

  # Install Poetry, a dependency management tool, in the container
  RUN pip install poetry

  # Copy the pyproject.toml and poetry.lock* files to the /app directory in the container
  # The poetry.lock* allows for an optional lock file, accommodating projects without one
  COPY pyproject.toml  poetry.lock* /app/

  # Export the dependencies from poetry to a requirements.txt file without hashes
  # This is needed for pip to understand and install the dependencies
  RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

  # Install the Python dependencies specified in requirements.txt
  # The `--no-cache-dir` flag prevents pip from caching installed packages, reducing image size
  RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

  # Copy the application source code from the local src directory to the /app directory in the container
  COPY ./src/ /app/

  # Define the default command to run when the container starts
  # This command runs the app using uvicorn with proxy headers enabled, listening on all network interfaces on port 80
  CMD ["uvicorn", "ska_cicd_training_pipeline_machinery.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80"]
  ```

- Add `.release` file to your root folder as it's needed to figure out the version for the OCI images. Make sure that the version in this file is aligned with the `pyproject.toml` file when it's first created.

  ```bash
  release=0.1.0
  tag=ska-cicd-training-pipeline-machinery-0.1.0
  ```

- Build the docker image

  ```bash
  make oci-build
  ```

  Get additional help and documentation for `oci` support by running `make help oci` or `make long-help oci`

- Add pipeline support for oci images by adding the following to your `includes` section in your `gitlab-ci-yml` file

  ```yaml
    # OCI
    - project: "ska-telescope/templates-repository"
      file: "gitlab-ci/includes/oci-image.gitlab-ci.yml"
  ```

  This will lint, build, scan for vulnerabilities and publish your OCI containers automatically.

- That's it! Congratulations!

The file structure of the repository after completing the Solution for Exercise 5 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-5-Solution)

### Exercise 6: Customize the Pipeline

**Note : The .gitlab-ci.yml file can also be customized as per the requirements. However, we do not recommend it and instead encourage to use makefile to make the customization as it runs the same way in pipeline as well as the developer's local environment.# Note : The .gitlab-ci.yml file can also be customized as per the requirements. However, we do not recommend it and instead encourage to use makefile to make the customization as it runs the same way in pipeline as well as the developer's local environment.**

For customizing the pipeline, let us consider the same example that we used in exercise 4 to disable pytest warnings.

- Remove the below line from makefile:

  ```bash
  PYTHON_VARS_AFTER_PYTEST=--disable-pytest-warnings
  ```

- Add the same line under "variables" section of .gitlab-ci.yml file:

  ```yaml
  variables:
  ...
  PYTHON_VARS_AFTER_PYTEST: --disable-pytest-warnings
  ```

This will use the customized pipeline instead of makefile.

The file structure of the repository after completing the Solution for Exercise 6 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-6-Solution)

### Exercise 7: Release a new version

As we do not recommend customizing the pipeline and instead encourage makefile customization, lets revert the changes done in Exercise before releasing a new version.

- Remove the below line from "variables" section of .gitlab-ci.yml file:

  ```yaml
  variables:
  ...
  PYTHON_VARS_AFTER_PYTEST: --disable-pytest-warnings
  ```

- Add the following line in the Makefile:

  ```bash
  PYTHON_VARS_AFTER_PYTEST= --disable-pytest-warnings
  ```

This will use the makefile instead of customized pipeline.

- Run `make help release` or `make long-help release` to see available documentation
- Check if a release exists

  ```bash
  $ make check-release
  ERROR: version not yet tagged in git. make [minor,major,patch]-release.
  make: *** [.make/release.mk:166: check-release] Error 1
  ```

- Bump minor version for docker and python and follow the output

  ```bash
  make bump-patch-release # set the .release file release values
  make set-release # set the versions of different files appropiately
  ```

  Note: If there are outstanding(uncommitted tracked) changes existing this will fail so commit first

- commit the changes for the version files

  ```bash
  make git-create-tag # call git commit and git tag
  make git-push-tag # push outstanding changes to git including tags.
  ```
- finally, include the release template for the pipeline

```yaml
  # Release
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/release.gitlab-ci.yml"
```

- This will create a GitLab release with the changelog, follow the Releases page in gitLab UI to see the details. In addition, if you have a `CHANGELOG` file in the repository it will get updated.

  Note: If there is same version tag existing this will fail so first delete the tag

  ```bash
  git tag -d <tag>
  ```

- If your push of tag fails that could be because the tag is already present on Gitlab or CAR. To fix this, just update your version to latest version in .release file and push it. It should pass then.

The file structure of the repository after completing the Solution for Exercise 7 can be found [here](https://gitlab.com/ska-telescope/ska-cicd-training-pipeline-machinery/-/tree/Tutorial-Exercise-7-Solution)

### Exercise 8: Add a Helm Chart

Helm charts are packages of pre-configured Kubernetes resources. They are used to describe, version, and deploy applications or services onto a Kubernetes cluster. They are the chosen artefact types for SKA to run on kubernetes environments.

So, let's wrap our application in a helm chart.

- Create a Helm Chart

```bash
mkdir -p charts
cd charts
helm create ska-cicd-training-pipeline-machinery
```

This will create a new directory with the structure of a Helm chart. Now we need to configure it for our application.

- Replace `image.repository` with `repository: artefact.skao.int/ska-cicd-training-pipeline-machinery`
- Delete `host` from `ingress` definition as it's not supported.
- Update the `appVersion` in the `Chart.yaml` file to the released version of the image from previous exercise. *If you are only doing this exercise, please check the .release file version in the repository and use this version*
- Add below `labels` to `values.yaml` file so that pipeline machinery can identify our deployments.
- Add namespace to the ingress to differentiate multiple deployments in `ingress.yaml` file: `- path: /{{ $.Release.Namespace }}{{ .path }}`. This will create a unique path for each deployment.
- Also add the following annotations and path changes to `ingress` in `values.yaml` file:

```yaml
  ingress:
  enabled: true
  className: ""
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
    # kubernetes.io/tls-acme: "true"
  hosts:
    - paths:
        - path: /(.*)
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local
```

```bash
labels:
  app: ska-cicd-training-pipeline-machinery
```

Final `ingress.yaml` should look like below:

```yaml
  {{- if .Values.ingress.enabled -}}
  {{- $fullName := include "ska-cicd-training-pipeline-machinery.fullname" . -}}
  {{- $svcPort := .Values.service.port -}}
  {{- if and .Values.ingress.className (not (semverCompare ">=1.18-0" .Capabilities.KubeVersion.GitVersion)) }}
    {{- if not (hasKey .Values.ingress.annotations "kubernetes.io/ingress.class") }}
    {{- $_ := set .Values.ingress.annotations "kubernetes.io/ingress.class" .Values.ingress.className}}
    {{- end }}
  {{- end }}
  {{- if semverCompare ">=1.19-0" .Capabilities.KubeVersion.GitVersion -}}
  apiVersion: networking.k8s.io/v1
  {{- else if semverCompare ">=1.14-0" .Capabilities.KubeVersion.GitVersion -}}
  apiVersion: networking.k8s.io/v1beta1
  {{- else -}}
  apiVersion: extensions/v1beta1
  {{- end }}
  kind: Ingress
  metadata:
    name: {{ $fullName }}
    labels:
      {{- include "ska-cicd-training-pipeline-machinery.labels" . | nindent 4 }}
    {{- with .Values.ingress.annotations }}
    annotations:
      {{- toYaml . | nindent 4 }}
    {{- end }}
  spec:
    {{- if and .Values.ingress.className (semverCompare ">=1.18-0" .Capabilities.KubeVersion.GitVersion) }}
    ingressClassName: {{ .Values.ingress.className }}
    {{- end }}
    {{- if .Values.ingress.tls }}
    tls:
      {{- range .Values.ingress.tls }}
      - hosts:
          {{- range .hosts }}
          - {{ . | quote }}
          {{- end }}
        secretName: {{ .secretName }}
      {{- end }}
    {{- end }}
    rules:
      {{- range .Values.ingress.hosts }}
      - http:
          paths:
            {{- range .paths }}
            - path: /{{ $.Release.Namespace }}{{ .path }}
              {{- if and .pathType (semverCompare ">=1.18-0" $.Capabilities.KubeVersion.GitVersion) }}
              pathType: {{ .pathType }}
              {{- end }}
              backend:
                {{- if semverCompare ">=1.19-0" $.Capabilities.KubeVersion.GitVersion }}
                service:
                  name: {{ $fullName }}
                  port:
                    number: {{ $svcPort }}
                {{- else }}
                serviceName: {{ $fullName }}
                servicePort: {{ $svcPort }}
                {{- end }}
            {{- end }}
      {{- end }}
  {{- end }}
```

- In order for these labels to be parsed correctly, we need to change our helm templates. Open the `charts/ska-cicd-training-pipeline-machinery/templates/_helpers.tpl` file and find where the common labels are define, `{{- define "ska-cicd-training-pipeline-machinery.labels" -}}` and add custom label support like below, *the actual changes are the `range` over `.Values.labels` but here the whole section is shown for better context*

```bash
{{/*
Common labels
*/}}
{{- define "ska-cicd-training-pipeline-machinery.labels" -}}
helm.sh/chart: {{ include "ska-cicd-training-pipeline-machinery.chart" . }}
{{ include "ska-cicd-training-pipeline-machinery.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- range $key, $value := .Values.labels }}
{{ $key }}: {{ $value | quote }}
{{- end }}
{{- end }}
```

- Now let's lint our chart.

```bash
cd .. # if you are in the charts/ folder
make helm-lint
...
1 chart(s) linted, 0 chart(s) failed
```

- Next, let's try to template it and build it to catch any other issues.

```bash
make k8s-template-chart
Setting CAR_OCI_REGISTRY_HOST to `artefact.skao.int`
Detected the SKA Tango Operator in the k8s cluster
k8s-dep-build: building dependencies
+++ Building ska-cicd-training-pipeline-machinery chart +++
template-chart: install ./charts/ska-cicd-training-pipeline-machinery/ release: test in Namespace: ska-cicd-training-pipeline-machinery with params: 
kubectl create ns ska-cicd-training-pipeline-machinery --dry-run=client -o yaml | tee manifests.yaml; \
helm template test \
 \
--debug \
 ./charts/ska-cicd-training-pipeline-machinery/ --namespace ska-cicd-training-pipeline-machinery | tee -a manifests.yaml
apiVersion: v1
kind: Namespace
metadata:
  creationTimestamp: null
  name: ska-cicd-training-pipeline-machinery
spec: {}
status: {}
install.go:200: [debug] Original chart version: ""
install.go:217: [debug] CHART PATH: /home/tango/ska-cicd-training-pipeline-machinery/charts/ska-cicd-training-pipeline-machinery

---
# Source: ska-cicd-training-pipeline-machinery/templates/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: test-ska-cicd-training-pipeline-machinery
  labels:
    helm.sh/chart: ska-cicd-training-pipeline-machinery-0.1.0
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
    app: "ska-cicd-training-pipeline-machinery"
---
# Source: ska-cicd-training-pipeline-machinery/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: test-ska-cicd-training-pipeline-machinery
  labels:
    helm.sh/chart: ska-cicd-training-pipeline-machinery-0.1.0
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
    app: "ska-cicd-training-pipeline-machinery"
spec:
  type: ClusterIP
  ports:
    - port: 80
      targetPort: http
      protocol: TCP
      name: http
  selector:
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
---
# Source: ska-cicd-training-pipeline-machinery/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-ska-cicd-training-pipeline-machinery
  labels:
    helm.sh/chart: ska-cicd-training-pipeline-machinery-0.1.0
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
    app: "ska-cicd-training-pipeline-machinery"
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
      app.kubernetes.io/instance: test
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
        app.kubernetes.io/instance: test
    spec:
      serviceAccountName: test-ska-cicd-training-pipeline-machinery
      securityContext:
        {}
      containers:
        - name: ska-cicd-training-pipeline-machinery
          securityContext:
            {}
          image: "artefact.skao.int/ska-cicd-training-pipeline-machinery:1.16.0"
          imagePullPolicy: IfNotPresent
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {}
---
# Source: ska-cicd-training-pipeline-machinery/templates/ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ska-cicd-training-pipeline-machinery
  labels:
    helm.sh/chart: ska-cicd-training-pipeline-machinery-0.1.0
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
    app: "ska-cicd-training-pipeline-machinery"
spec:
  rules:
    - host: 
      http:
        paths:
          - path: /ska-cicd-training-pipeline-machinery/
            pathType: ImplementationSpecific
            backend:
              service:
                name: test-ska-cicd-training-pipeline-machinery
                port:
                  number: 80
---
# Source: ska-cicd-training-pipeline-machinery/templates/tests/test-connection.yaml
apiVersion: v1
kind: Pod
metadata:
  name: "test-ska-cicd-training-pipeline-machinery-test-connection"
  labels:
    helm.sh/chart: ska-cicd-training-pipeline-machinery-0.1.0
    app.kubernetes.io/name: ska-cicd-training-pipeline-machinery
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
    app: "ska-cicd-training-pipeline-machinery"
  annotations:
    "helm.sh/hook": test
spec:
  containers:
    - name: wget
      image: busybox
      command: ['wget']
      args: ['test-ska-cicd-training-pipeline-machinery:80']
  restartPolicy: Never
```

- Commit the changes and push.
- _(Optional)_ If you have a local kubernetes cluster you can access, you can deploy locally by running `make k8s-install-chart` target.
- We need to add the gitlab templates to have the Helm support in the pipelines. Add the following under `include` directive in `.gitlab-ci.yml` file.

```bash
  # Helm
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/helm-chart.gitlab-ci.yml"
  # K8s
  # This is to make sure our tests are running inside the runner pod instead of the legacy way
  # that k8s.gitlab-ci-yml file provides
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/k8s-test-runner.gitlab-ci.yml"
```

- Commit & Push your changes to see the pipeline using helm and k8s jobs. *Note: `k8s-test-runner` job will run the same tests as before as we don't have any actual tests at this stage yet. Actual integration tests will be added in the next exercise*

You may see that the tests are passing even though the deployment is failing if you are not using a released version like below.

```bash
...
Error from server (BadRequest): container "ska-cicd-training-pipeline-machinery" in pod "test-ska-cicd-training-pipeline-machinery-84bbcb686f-nd9d4" is waiting to start: trying and failing to pull image
make: *** [.make/k8s.mk:579: k8s-do-test-runner] Error 1
```

This is because we are running the same unit tests inside this integration test job and if we check the templated chart, it can be seen that the the image repository and tag do not exist.

- For CI, it's best to use the newly built image from the previous stage. To be able to dynamically set the image for the helm chart to use, include the following check in your `Makefile`. This will only run if it's inside a gitlab runner job (due to the presence of `CI_JOB_ID` variable) so that in any other environment, such as minikube or main branch/staging deployments, it will run the released image version.

```bash
make long-help k8s # To get help on the variables we need to provide

# Override the defualt image repository and tag to always use the previously built image in pipelines
ifneq ($(CI_JOB_ID),)
K8S_CHART_PARAMS = --set image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
 --set image.repository=$(CI_REGISTRY)/ska-telescope/ska-cicd-training-pipeline-machinery
endif
```

### Exercise 9: Add Integration Tests

Let's fix the integration tests by actually talking to our deployed application instead of running it locally.

- Add a new `tests/integration` folder to hold integration tests. Move the existing unit tests to a new `tests/unit` folder to better manage different tests.

```bash
mkdir tests/unit
touch tests/unit/__init__.py
mkdir tests/integration
touch tests/integration/__init__.py
mv tests/test_main.py tests/unit/test_main.py
make python-test # to ensure unit tests are still working
```

- Add `requests` library to our dependencies.
-

```bash
poetry add --group dev requests
```

- Create a new file `test_integration.py` under `tests/integration` and write a new test.

```bash
touch tests/integration/test_integration.py
```

```python
"""
Simple FASTAPI App Tests for workshop purposes

Returns:
  None
"""

import pytest
import requests


@pytest.mark.post_deployment
def test_root_endpoint():
    """
    Tests the root endpoint of the deployed FastAPI application.

    Sends a GET request to the root endpoint and verifies
    the response matches the expected status code and message.
    """
    # URL of the service, assuming the service is exposed externally
    url = "http://test-ska-cicd-training-pipeline-machinery:80/"

    # Sending a GET request to the root endpoint
    response = requests.get(url, timeout=10)

    # Verifying the status code
    assert (
        response.status_code == 200
    ), f"Expected status code 200, got {response.status_code}"

    # Verifying the response body
    expected_message = {"message": "Hello World"}
    assert (
        response.json() == expected_message
    ), f"Expected message {expected_message}, got {response.json()}"


```

- Run `make python-test` to ensure we don't have any regression.

The integration test will probably fail due to a lack of commincation error as we don't have a deployment of our appliation helm chart locally or it's not exposed for the test to run.

- Add another pytest argument to ignore integration tests. *There are different ways to achieve the same result by using different variables so the below approach is only one of them*

```bash
PYTHON_VARS_AFTER_PYTEST=-m 'not post_deployment' --disable-pytest-warnings

k8s-test: PYTHON_VARS_AFTER_PYTEST := \
-m 'post_deployment' --disable-pytest-warnings
```

You'll see now that the unit tests are passing while the integration tests are deselected.

- Commit and observe the test run  by calling `make k8s-test` against your deployment. You can deploy into the minikube environment by calling `make k8s-install-chart`.

### Exercise 10: Centrealised Monitoring and Logging

- First let's add the development templates so that we can create on-demand deployments.

```yaml
  # Helm
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/deploy.gitlab-ci.yml"
```

Now let's use central monitoring tooling, kibana, to find the logs of our deployment.

- Go to <https://k8s.stfc.skao.int/kibana> to access kibana and filter on your own CI namespace
- Go to <https://monitoring.skao.int> to access grafana and filter on your own CI namespace

The more up to date information on monitoring and logging can be found on [the developer portal](https://developer.skao.int/en/latest/tools/centralised-monitoring-and-logging.html).

The main takeaways/dashboards/views to look are highlighted below but the instructions to find them should be found either in developer portal or by self exploration.

- **Kibana**
  - Logs
    - Filter on your namespace
    - Filter on your deployment
    - Filter on your pod
    - Filter on your log level
  - Discover
    - Filter on your namespace
    - Filter on your deployment
    - Filter on your pod
    - Filter on your log severity
- **Grafana**
  - Dashboards
    - Kubernetes / Compute Resources / Namespace (Pods)
    - Kubernetes / Compute Resources / Namespace (Workloads)
    - Kubernetes / Compute Resources / Pod or Workload
    - Kubernetes / Compute Resources / DeviceServer (if you have Tango Devices)
